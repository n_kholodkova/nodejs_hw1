const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const cors = require('cors');
const path = require('path');

const {
  getFiles,
  writeFile, updateFile, deleteFile,
  getFile,
} = require('./helpers');

const expressApp = express();
const portNumber = '8080';

try {
  fs.mkdirSync(path.normalize(path.join(__dirname, 'api/files')), { recursive: true });
} catch (error) {
  console.log(error);
}

expressApp.use(cors());
expressApp.use(express.json());
expressApp.use(morgan('common', {
  stream: fs.createWriteStream('./logs.log', { flags: 'a' }),
}));
expressApp.use(morgan('dev'));

expressApp.get('/api/files', (request, response) => {
  try {
    getFiles(request, response);
  } catch (error) {
    response.status(500).json({ message: 'Server error' });
  }
});

expressApp.post('/api/files', (request, response) => {
  try {
    writeFile(request, response);
  } catch (error) {
    response.status(500).json({ message: 'Server error' });
  }
});

expressApp.get('/api/files/:filename', (request, response) => {
  try {
    getFile(request, response);
  } catch (error) {
    response.status(500).json({ message: 'Server error' });
  }
});

expressApp.put('/api/files/:filename', (request, response) => {
  try {
    updateFile(request, response);
  } catch (error) {
    response.status(400).json({ message: error.message });
  }
});

expressApp.delete('/api/files/:filename', (request, response) => {
  try {
    deleteFile(request.trim(), response);
  } catch (error) {
    response.status(400).json({ message: error.message });
  }
});

expressApp.all('/api/files', (request, response) => {
  response.status(400).json({ error: 'Invalid request method' });
});

expressApp.listen(portNumber, () => console.log(`Server is working now on http://localhost:${portNumber}. Good luck :)`));
