const fs = require('fs');
const path = require('path');

const getPath = (fileName) => path.normalize(path.join(__dirname, `api/files/${fileName}`));

const ifFileExists = (fileName) => fs.existsSync(getPath(fileName));

const ifDirExists = () => fs.existsSync(path.normalize(path.join(__dirname, 'api/files')));

const getFiles = (request, response) => {
  try {
    const files = fs.readdirSync(path.normalize(path.join(__dirname, 'api/files')));
    response.status(200).json({
      message: 'Success',
      files,
    }).end();
  } catch (error) {
    response.status(400).json({
      message: 'Client error',
    }).end();
  }
};

const getFile = (request, response) => {
  const { filename: fileName } = request.params;
  if (!fileName) {
    return response.status(400).json({ message: 'Client error' });
  }
  if (!ifFileExists(fileName)) {
    return response.status(400).json({ message: 'Client error' });
  }
  fs.readFile(getPath(fileName), 'utf-8', (error, data) => {
    if (error) {
      return response.status(500).json({ message: 'Server error' });
    }
    const extension = fileName.split('.').slice(-1)[0];
    fs.stat(getPath(fileName), (_, stats) => {
      const uploadedDate = new Date(stats.birthtimeMs);
      return response.status(200).json({
        message: 'Success',
        filename: fileName,
        content: data,
        extension,
        uploadedDate,
      });
    });
  });
};

const writeFile = (request, response) => {
  const { filename: fileName, content } = request.body;
  if (!fileName) {
    return response.status(400).json({ message: 'Client error' });
  }
  if (!content) {
    return response.status(400).json({ message: 'Client error' });
  }
  if (ifFileExists(fileName)) {
    return response.status(400).json({ message: 'Client error' });
  }
  fs.writeFile(getPath(fileName), content, (error) => {
    if (error) {
      return response.status(500).json({ message: 'Server error' });
    }
    return response.status(200).json({
      message: 'File created successfully',
    });
  });
};

const updateFile = (request, response) => {
  const { content } = request.body;
  const { filename: fileName } = request.params;
  if (!fileName) {
    return response.status(400).json({ message: 'Client error' });
  }
  if (!content) {
    return response.status(400).json({ message: 'Client error' });
  }
  fs.writeFile(getPath(fileName), content, (error) => {
    if (error) {
      return response.status(500).json({ message: 'Server error' });
    }
    return response.status(200).json({
      message: 'Success',
    });
  });
};

const deleteFile = (request, response) => {
  const { filename: fileName } = request.params;
  if (!fileName) {
    return response.status(400).json({ message: 'Client error' });
  }
  if (!ifFileExists(fileName)) {
    return response.status(400).json({ message: 'Client error' });
  }
  fs.unlink(getPath(fileName), (error) => {
    if (error) {
      return response.status(500).json({ message: 'Server error' });
    }
    return response.status(200).json({
      message: 'Success',
    });
  });
};

module.exports.getFiles = getFiles;
module.exports.writeFile = writeFile;
module.exports.deleteFile = deleteFile;
module.exports.updateFile = updateFile;
module.exports.getFile = getFile;
