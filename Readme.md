# Very simple server with Node.js and Express.js which works with files (you can upload, delete, update and read).

### Routes:
- #### GET

  - /api/files/:filename

    return file content if exists, otherwise error message and status code 400, otherwise success message and code 200.

  - /api/files

    return list of files which were uploaded or status code 400 and error message, otherwise success message and code 200.
- #### POST

  - /api/files

    in body of request gets parameters as json in next format {filename: "NAME OF FILE", content: "FILE'S CONTENT"}. Return error and status code 400 if file already exists or request content-type is not application/json, otherwise success message and code 200.

- #### PUT

  - /api/files/:filename

    in body of request gets parameters as json in next format {filename: "NAME OF FILE", content: "FILE'S CONTENT"}. Return error status code 400 if file does not exist or request content-type is not application/json. Otherwise success message and code 200.
- #### DELETE

  - /api/files/:filename

    return error status code 400 if file does not exist, otherwise success message and code 200.

To run use `npm install` and `npm start`
